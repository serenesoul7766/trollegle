package anon.trollegle;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ban {
    public enum Action {
        PASS(), 
        SOFT(PASS), 
        WARN(PASS, SOFT), 
        MUTE(PASS, SOFT, WARN), 
        KICK(PASS, SOFT, WARN),
        /** Mute the user and suppress any command on first mute */
        MUTE_ALL(PASS, SOFT, WARN, MUTE);
        
        // All this because EnumSet doesn't like being constructed in its element type's constructor
        private int lesser;
        Action(Action... lesser) {
            for (Action a : lesser)
                this.lesser |= 1 << a.ordinal();
        }
        public boolean supersedes(Action other) {
            return (lesser & 1 << other.ordinal()) != 0;
        }
        public boolean isHighest() {
            return (highest & 1 << ordinal()) != 0;
        }
        
        private static int highest = ~0;
        static {
            for (Action action : Action.values())
                highest &= ~action.lesser;
        }
    };
    
    public static final int 
        DEFAULT_PROBATION = 0,
        ALWAYS_VALID = -1;
    public static final double
        DEFAULT_LIKELIHOOD = -1,
        ALWAYS_KICK = 0,
        ALWAYS_MUTE = 1;
    private static final Predicate<Matcher>
        matches = Matcher::matches,
        lookingAt = Matcher::lookingAt,
        find = Matcher::find;

    private boolean enabled = true;
    private int probation;
    private long probationTime;
    private double muteLikelihood = DEFAULT_LIKELIHOOD;
    private boolean soft, warn, mutecmds, excludePulse, excludeQuestion, excludeText;
    private Predicate<Matcher> matchType = matches;
    public final String pattern;
    private final Pattern compiledPattern;
    public final String comment;
    
    public static final String
        ALWAYS = "always",
        SOFT = "soft",
        HARD = "hard",
        KICK = "kick",
        MUTE = "mute",
        MUTECMDS = "mutecmds",
        NOMUTECMDS = "nomutecmds",
        WARN = "warn",
        NOWARN = "nowarn",
        OFF = "off",
        ON = "on",
        MATCHES = "matches",
        LOOKINGAT = "lookingat",
        FIND = "find",
        NOPULSE = "nopulse",
        PULSE = "pulse",
        NOQUESTION = "noquestion",
        QUESTION = "question",
        NOTEXT = "notext",
        TEXT = "text",
        FOREVER = "forever";
    
    public Ban(String pattern) {
        if (pattern == null)
            throw new NullPointerException();
            
        while (pattern.length() > 2 && pattern.charAt(0) == '?') {
            String[] parts = pattern.split(" ", 2);
            if (parts.length < 2)
                break;
            pattern = parts[1];
            setOption(parts[0]);
        }
        int commentStart = pattern.lastIndexOf(" # ");
        if (commentStart != -1) {
            comment = pattern.substring(commentStart + 3);
            pattern = pattern.substring(0, commentStart);
        } else {
            comment = null;
        }
        this.pattern = pattern;
        compiledPattern = Pattern.compile(pattern, Pattern.DOTALL);
    }
    
    protected void setOption(String option) {
        if (pattern != null)
            throw new IllegalStateException("ban already constructed");
        if (option.length() < 2)
            return;
        if (option.charAt(0) == '?')
            option = option.substring(1);
        if (option.charAt(0) == '?')
            return;
        
        if (option.equals(ALWAYS))
            probation = ALWAYS_VALID;
        if (option.equals(FOREVER))
            probationTime = ALWAYS_VALID;
        else if (option.equals(SOFT))
            soft = true;
        else if (option.equals(HARD))
            soft = false;
        else if (option.equals(KICK))
            muteLikelihood = ALWAYS_KICK;
        else if (option.equals(MUTE))
            muteLikelihood = ALWAYS_MUTE;
        else if (option.equals(MUTECMDS))
            mutecmds = true;
        else if (option.equals(NOMUTECMDS))
            mutecmds = false;
        else if (option.equals(WARN))
            warn = true;
        else if (option.equals(NOWARN))
            warn = false;
        else if (option.equals(OFF))
            enabled = false;
        else if (option.equals(ON))
            enabled = true;
        else if (option.equals(MATCHES))
            matchType = matches;
        else if (option.equals(LOOKINGAT))
            matchType = lookingAt;
        else if (option.equals(FIND))
            matchType = find;
        else if (option.equals(NOPULSE))
            excludePulse = true;
        else if (option.equals(PULSE))
            excludePulse = false;
        else if (option.equals(NOQUESTION))
            excludeQuestion = true;
        else if (option.equals(QUESTION))
            excludeQuestion = false;
        else if (option.equals(NOTEXT))
            excludeText = true;
        else if (option.equals(TEXT))
            excludeText = false;
        else if (option.matches("-?[0-9]+"))
            probation = Integer.parseInt(option);
        else if (option.matches("-?[0-9]+\\.[0-9]+"))
            muteLikelihood = Double.parseDouble(option);
        else if (option.matches("[Tt](?:-?[0-9]+(?:.[0-9]+)?|hr?|hrs|mn?|mins?|m?s|secs?)+"))
            probationTime = Util.parseTime(option.substring(1));
    }
    
    private StringBuilder a(StringBuilder sb, Object option) {
        return sb.append("?").append(option).append(" ");
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!enabled)
            a(sb, OFF);
        
        if (soft)
            a(sb, SOFT);
        else if (warn)
            a(sb, WARN);
        
        if (muteLikelihood == ALWAYS_KICK) {
            a(sb, KICK);
        } else {
            if (muteLikelihood == ALWAYS_MUTE)
                a(sb, MUTE);
            else if (muteLikelihood != DEFAULT_LIKELIHOOD)
                a(sb, Double.toString(muteLikelihood));
            if (mutecmds)
                a(sb, MUTECMDS);
        }
        
        if (probation == ALWAYS_VALID)
            a(sb, ALWAYS);
        else if (probation != DEFAULT_PROBATION)
            a(sb, Integer.toString(probation));
        
        if (matchType == lookingAt)
            a(sb, LOOKINGAT);
        else if (matchType == find)
            a(sb, FIND);
        
        if (excludePulse)
            a(sb, NOPULSE);
        
        if (excludeQuestion)
            a(sb, NOQUESTION);
        
        if (excludeText)
            a(sb, NOTEXT);
        
        if (probationTime == ALWAYS_VALID)
            a(sb, FOREVER);
        else if (probationTime != DEFAULT_PROBATION)
            a(sb, "t" + Util.minSec(probationTime).replace(" ", ""));
        
        sb.append(pattern);
        if (comment != null)
            sb.append(" # ").append(comment);
        return sb.toString();
    }
    
    public boolean isEnabled() { return enabled; }
    
    private Action mute() {
        return mutecmds ? Action.MUTE_ALL : Action.MUTE;
    }
    
    public Action check(Multi multi, MultiUser user, String message) {
        if (!enabled)
            return Action.PASS;
        if (excludePulse && user != null && user.isPulseEver())
            return Action.PASS;
        if (excludeQuestion && user != null && user.isQuestionMode())
            return Action.PASS;
        if (excludeText && user != null && !user.isPulseEver() && !user.isQuestionMode())
            return Action.PASS;
        if (user != null) {
            long effectiveProbationTime = probationTime == DEFAULT_PROBATION 
                ? multi.banSpanTime : probationTime;
            if (effectiveProbationTime != ALWAYS_VALID && user.age() >= effectiveProbationTime)
                return Action.PASS;
            int effectiveProbation = probation == DEFAULT_PROBATION ? multi.banSpan : probation;
            if (effectiveProbation != ALWAYS_VALID && user.getMsgCount() >= effectiveProbation)
                return Action.PASS;
        }
        if (!matchType.test(compiledPattern.matcher(message)))
            return Action.PASS;
        if (soft)
            return Action.SOFT;
        if (warn && user != null && !user.isWarned())
            return Action.WARN;
        if (muteLikelihood == ALWAYS_KICK)
            return Action.KICK;
        if (muteLikelihood == ALWAYS_MUTE)
            return mute();
        if (muteLikelihood == DEFAULT_LIKELIHOOD)
            return Math.random() > multi.muteBotFreq ? Action.KICK : mute();
        return Math.random() > muteLikelihood ? Action.KICK : mute();
    }
    
}
