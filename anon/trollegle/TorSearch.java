package anon.trollegle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;

/**
 * A thread that checks whether a proxy can be used to open an Omegle connection.
 * If the proxy is a Tor instance, this is repeated with new exit nodes until a usable one is found.
 *
 * This explicitly uses two platform threads so that attempts to connect to unreliable proxies don't
 * starve the pool of carrier threads.
 */
public class TorSearch extends Thread implements Callback<UserConnection> {

    private long start;
    private boolean qShown;
    private boolean accepted;
    private boolean gotTyping;
    private boolean firstRun = true;
    private ProxyConfig pc;
    private Callback<?> callback;

    public void callback(UserConnection user, String method, int data) {
        // nop
    }

    @Override
    public void callback(final UserConnection user, String method, String data) {
        if (method.equals("captcha")) {
            log("captcha: " + data);
            accepted = false;
            user.dispose();
        } else if (method.equals("ban")) {
            log("ban: " + data);
            accepted = false;
            user.dispose();
        } else if (method.equals("died")) {
            log("died: " + data);
            accepted = false;
            user.dispose();
        } else if (method.equals("disconnected")) {
            log("Stranger has disconnected");
            accepted = true;
            user.dispose();
        } else if (method.equals("typing")) {
            gotTyping = true;
        } else if (method.equals("stoppedtyping")) {
            if (gotTyping) {
                user.schedSend(Util.chooseWord());
            } else {
                accepted = true;
                user.sendDisconnect();
                user.dispose();
            }
        } else if (method.equals("message")) {
            hear(user, data);
        } else if (method.equals("connected") || method.equals("accepted")) {
            if (!qShown) {
                qShown = true;
                log("Connected");
            }
            Util.makeThread(() -> {
                Util.sleep((long) (Math.random() * 120 * 1000));
                if (!user.isAlive() || gotTyping) return;
                user.schedSendTyping(false);
            }).start();
        } else if (method.equals("pulse success")) {
            // nop
        } else if (method.equals("chatname")) {
            // nop
        } else {
            System.err.println("\n\n\nUNKNOWN CALLBACK METHOD '" + method + "'\n\n\n");
        }
    }

    private boolean nextCircuit() {
        if (pc.getTorControl() == null)
            return false;
        // Tor allows at most one newnym per 10 seconds
        Util.sleep(firstRun ? (long) (Math.random() * 10000)
                : Math.max(500, 10000l - System.currentTimeMillis() + start));
        firstRun = false;
        try (Socket socket = new Socket()) {
            socket.connect(pc.getTorControl());
            try (
                    PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
                    BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()))
            ) {
                pw.println("authenticate \"\"");
                if (!check250(br)) return false;
                pw.println("signal newnym");
                if (!check250(br)) return false;
                pw.println("quit");
                if (!check250(br)) return false;
            }
        } catch (IOException e) {
            e.printStackTrace(); // XXX
            return false;
        }
        pc.clearCheckCode();
        return true;
    }
    
    private boolean check250(BufferedReader reader) throws IOException {
        return reader.readLine().startsWith("250");
    }
    
    public TorSearch(ProxyConfig pc, Callback<?> callback) {
        this.pc = pc;
        this.callback = callback;
    }
    
    public void run() {
        do {
            start = System.currentTimeMillis();
            accepted = false;
            qShown = false;
            gotTyping = false;
            UserConnection user = new UserConnection(this) {
                boolean firstThread = true;
                public String getDisplayNick() {
                    return getProxy().toString();
                }
                protected void waiting() {
                    stopLookingForCommonLikes();
                }
                protected Thread makeThread(Runnable task) {
                    if (firstThread) {
                        firstThread = false;
                        return new Thread(task);
                    }
                    return super.makeThread(task);
                }
            }.fill(false, pc)
             .withTopics(Util.chooseWords(10));
            user.start();
            try {
                user.join(1000 * 60 * 7);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            if (user.isAlive()) {
                if (user.isReady())
                    accepted = true;
                user.sendDisconnect();
                user.dispose();
            }
            if (accepted && !isUnique())
                accepted = false;
        } while (pc.isEnabled() && !accepted && nextCircuit());
        if (accepted) {
            pc.unban();
            callback.callback(null, "tor found", pc.getProxy().toString());
        } else if (!pc.isEnabled()) {
            callback.callback(null, "tor aborted", pc.getProxy().toString());
        } else {
            callback.callback(null, "tor failed", pc.getProxy().toString());
        }
    }
    
    /** NASTY NASTY SIDE EFFECT: sets exitAddress on pc */
    private boolean isUnique() {
        String line = pc.checkExitAddress();
        if (line == null) {
            UserConnection.log(pc + " exit ip check: line is null");
            return true;
        }
        try {
            callback.callback(null, "tor duplicate", line);
            pc.exitAddress = line;
            return true;
        } catch (Exception e) {
            if (e.getMessage().equals("nah")) {
                UserConnection.log(pc + " has duplicate exit ip " + line);
                return false;
            }
            UserConnection.log(pc + " exit ip check: " + e);
            return true;
        }
    }

    private void hear(UserConnection user, String data) {
        log("Stranger: " + data);
        if (Util.isTailEat(data)) {
            accepted = true;
            user.sendDisconnect();
            user.dispose();
        }
        if (data.matches("(?i)hi+|hell+o+|he+y+|a+y+"))
            say(user, Math.random() > 0.5 ? "hi" : "hello");
        else if (data.matches("(?i)w*h*a*t*'*s* *y*o*u+r+ n+a+m+e+[^A-Za-z]*"))
            say(user, Util.chooseName());
        else
            say(user, Util.chooseWord());
    }

    private void say(UserConnection user, String data) {
        //log("You: " + data);
        user.schedSend(data);
    }
    
    private void log(Object message) {
        UserConnection.logVerbose(pc + " " + message);
    }
    
    public boolean isAccepted() {
        return accepted;
    }

}
